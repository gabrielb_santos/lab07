/**
* @file	 	q4.cpp
* @brief	Programa que imprime os números primos entre 1 e um número
*			passado pelo usuário.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	08/06/2017
* @date		08/06/2017
*/


#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;


/** 
 * @brief	Função que verifica se um número é primo ou não.
 * @param	n N número que será testado
 * @return  true para número primo, false para não primo.
 */
bool show_primos(int n){
	int cont = 0;

	if(n == 1 || n == 2)
		return true;

	for (int i = 1; i <= n; ++i)
	{
		if(n%i == 0){
			cont++;
		}
	}

	if(cont == 2){
		return true;
	}else{
		return false;
	}
}

int main(int argc, char const *argv[]) {

	int num;
	num =  strtol(argv[1], NULL, 10);

	vector<int> m;
	vector<int>::iterator it;
	

	for (int i = 1; i <= num; ++i){
		m.push_back(i);	
	}


	cout << "Numeros primos [1-" << num <<"]: ";
	for (it = m.begin(); it != m.end(); ++it)
	{
		
		if(show_primos(*it) == true){
 	 		cout << *it << ' ';
 	 	}

	}
	

	return 0;
}