/**
* @file	 	q2.cpp
* @brief	Programa que imprime todos os valores de um container
* 			a impressão é feita juntamente com um "label" e um "separator"
*			passados como parâmetro na chamada da função.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	07/06/2017
* @date		08/06/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include <set> 
using std::set;


/** 
 * @brief	Função que imprime todos os valores de um container,
 * 			de um label e de separators.
 * @param	collection COLLECTION container que será impresso
 * @param	label LABEL string que será impressa no início do programa.
 * @param	separator SEPARATOR character impresso entre os valores do container.
 */
template<typename TContainer>
void print_elementos(const TContainer& collection, const char* label="", const char separator=' ')
{
	cout << label;
	typename TContainer::const_iterator it = collection.begin();	
	unsigned int i = 0;

	for (; it != collection.end(); ++it)
	{
		cout << *it;

		if(i + 1 < collection.size()){
			cout << separator;
		}

		i++;
	}

	cout << endl;

}

int main() {

	set<int> numeros;
	numeros.insert(10);
	numeros.insert(1);
	numeros.insert(4);
	numeros.insert(1);
	numeros.insert(2);
	numeros.insert(5);

	print_elementos(numeros, "Set: ");
	print_elementos(numeros, "CSV Set: ", ';');
	return 0;
}