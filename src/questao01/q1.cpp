/**
* @file	 	q1.cpp
* @brief	Programa que retorna um iterador para o elemento num intervalo
*			cujo valor é o mais próximo da média do intervalo.
* @author	Gabriel Barbosa (gbsbarbosa.gb@gmail.com)
* @since	07/06/2017
* @date		08/06/2017
*/

#include <iostream>
using std::cout;
using std::endl;

#include <vector>
using std::vector;


/** 
 * @brief	Função que realiza o cálculo do elemento com valor 
 *			mais próximo da média do intervalo.
 * @param	first FIRST iterador que aponta para o primerio elemento do intervalo
 * @param	last LAST iterador que aponta para o último elemento do intervalo 
 * @return 	iterados para o elemento mais próximo média.
 */
template<typename InputIterator>
InputIterator closest2mean(InputIterator first, InputIterator last){
	int media = 0;
	int count = 0;

	for (InputIterator i = first; i != last; ++i)
	{
		media += *i;
		count++;
	}

	media = media / count;

	unsigned int diff[2];
	diff[0] = *first - media;

	InputIterator close;
	for (InputIterator i = first+1; i != last; ++i)
	{
		diff[1] = *i - media;
		if (diff[1] < diff[0])
		{
			close = i;
			diff[0] = diff[1];
		}
		
	}

	return  close;

}

int main(){
	system("clear");
	
	vector<int> v { 1, 2, 3, 30, 40, 50};
	auto result = closest2mean(v.begin(), v.end());
	cout << (*result) << endl;
	return 0;

}