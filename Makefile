q1 = ./bin/q1

q2 = ./bin/q2

q3 = ./bin/q3

q4 = ./bin/q4

INC_DIR = ./include
SRC_DIR = ./src
OBJ_DIR = ./build
BIN_DIR = ./bin
DOC_DIR = ./doc

CC = g++

RM = rm -rf

OBJS_Q01 = ./build/questao01/q1.o

OBJS_Q02 = ./build/questao02/q2.o

OBJS_Q03 = ./build/questao03/q3.o

OBJS_Q04 = ./build/questao04/q4.o

CPPFLAGS = -Wall -pedantic -ansi -std=c++11

.PHONY: init all questao01  questao02 questao03 questao04 doc clean 

all: questao01 questao02 questao03 questao04


debug: CFLAGS += -g -O0
debug: all


questao01: $(q1) 

$(q1): $(OBJS_Q01)
	$(CC) $(OBJS_Q01) $(CPPFLAGS) $(INC_01) -o $@

$(OBJ_DIR)/questao01/q1.o: $(SRC_DIR)/questao01/q1.cpp
	$(CC) -c $(CPPFLAGS) $^ -o $@


questao02: $(q2)

$(q2):	$(OBJS_Q02)
	$(CC) $^ $(CPPFLAGS) $(INC_02) -o $@

$(OBJ_DIR)/questao02/q2.o: $(SRC_DIR)/questao02/q2.cpp
	$(CC) -c $(CPPFLAGS) $^ -o $@


questao03: $(q3)

$(q3): $(OBJS_Q03)
	$(CC) $(OBJS_Q03) $(CPPFLAGS) $(INC_03) -o $@

$(OBJ_DIR)/questao03/q3.o: $(SRC_DIR)/questao03/q3.cpp
	$(CC) -c $(CPPFLAGS) $^ -o $@


questao04: $(q4)

$(q4): $(OBJS_Q04)
	$(CC) $(OBJS_Q04) $(CPPFLAGS) $(INC_04) -o $@

$(OBJ_DIR)/questao04/q4.o: $(SRC_DIR)/questao04/q4.cpp
	$(CC) -c $(CPPFLAGS) $^ -o $@

doxy:
	doxygen Doxyfile

doc:
	$(RM) $(DOC_DIR)/*

clean:
	$(RM) $(OBJ_DIR)/questao01/* $(OBJ_DIR)/questao02/* $(OBJ_DIR)/questao03/* $(OBJ_DIR)/questao04/*
	$(RM) $(BIN_DIR)/*
